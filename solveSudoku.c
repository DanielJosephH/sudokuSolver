#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#include "sudokuStructures.h"

#define DEBUG false

cell cells[9][9];

bool checkCellLegal(int x, int y, int value) {
  // check that the value of the cell does not occur again in the row/ column/ box.
  for (int r = 0; r < 9; r++) {
    // check column
    if (cells[r][x].value == value && r != y) {
      if (DEBUG) {
        printf("Illegal cell co-ords are: %d %d\n", x, r);
      }
      return false;
    }
    // check row
    if (cells[y][r].value == value && r != x) {
      if (DEBUG) {
        printf("Illegal cell co-ords are: %d %d\n", r, y);
      }
      return false;
    }
    // check box
    // checkX loops from the left most box (lowest x index in box) to left plus 3.
    int checkX = (x - (x % 3)) + (r % 3);
    // checkY increases from top most row (lowest y index in box) to top plus 3.
    int checkY = (y - (y % 3)) + (int) floor(r/3);
    if (cells[checkY][checkX].value == value && checkX != x && checkY != y) {
      if (DEBUG) {
        printf("Illegal cell co-ords are: %d %d\n", checkX, checkY);
      }
      return false;
    }
  }
  return true;
}

int main(int argc, char const *argv[]) {

  // read the file contents into the data structure.
  FILE *fp = fopen("testSudoku1.txt", "r");
  if (!fp) {
    printf("ERROR: file null pointer\n");
  }

  // show the initial grid with + showing corners of boxes for easy distinction.
  printf("INITIAL GRID\n");
  for (int i = 0; i < 9; i++){
    if (i % 3 == 0) {
      printf("+-----------+-----------+-----------+\n");
    } else {
      printf("-------------------------------------\n");
    }
    printf("|");

    for (int q = 0; q < 9; q++){

      fscanf(fp, "%d", &cells[i][q].value);
      if (cells[i][q].value != 0) {
        cells[i][q].fixed = true;
      }
      printf(" %d |", cells[i][q].value);
    }
    printf("\n");
  }
  printf("+-----------+-----------+-----------+\n");

  fclose(fp);


  // itterate through cells using  a depth-first approach, backtrack only when all options in a cell tried.
  int currentX = 0, currentY = 0;
  while (true) {

    // prints each itteration of the sudoku.
    if (DEBUG) {
      for (int i = 0; i < 9; i++){
        for (int q = 0; q < 9; q++){
          printf("%d ", cells[i][q].value);
        }
        printf("\n");
      }
      printf("\n\n");
    }

    // if cell is fixed then skip to next cell.
    if (cells[currentY][currentX].fixed) {

      currentX++;
      // check cell is not out of bounds
      if (currentX >= 9) {
        currentX = 0;
        currentY++;
        if (currentY >= 9) {
          break;
        }
      }

    } else {

      // increment value of current cell, check value bounds.
      cells[currentY][currentX].value++;
      // if the cells value has exceeded 9 then set back to 0 and go back to the previous cell that is less than 9.
      if (cells[currentY][currentX].value >= 10) {

        while (cells[currentY][currentX].value >= 10) {

            cells[currentY][currentX].value = 0;

            // move to previous non-fixed cell, skip fixed cells.
            do {
              if (DEBUG) {
                printf("MOVED to previous cell\n");
              }
              currentX--;
              if (currentX <= -1) {
                currentX = 8;
                currentY--;
                if (currentY <= -1) {
                  printf("ERROR: cell out of bounds\n");
                  break;
                }
              }
            } while (cells[currentY][currentX].fixed);
        }

      } else {
        // check if new cell value is legal, if not loop back to top.
        if (!checkCellLegal(currentX, currentY, cells[currentY][currentX].value)) {
          continue;
        } else {
          // cell is legal, move onto next cell.
          currentX++;
          if (currentX >= 9) {
            currentX = 0;
            currentY++;
            if (currentY >= 9) {
              break;
            }
          }
        }
      }
    }
  }

  // print solution
  printf("\nSOLUTION GRID\n");
  for (int i = 0; i < 9; i++){
    if (i % 3 == 0) {
      printf("+-----------+-----------+-----------+\n");
    } else {
      printf("-------------------------------------\n");
    }
    printf("|");

    for (int q = 0; q < 9; q++){
      printf(" %d |", cells[i][q].value);
    }
    printf("\n");
  }
  printf("+-----------+-----------+-----------+\n");

  return 0;
}
